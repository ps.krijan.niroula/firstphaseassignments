package com.clusus.exception;

public class MaximumSizeExceedException extends RuntimeException {

    public MaximumSizeExceedException(String message) {
        super(message);
    }
}
