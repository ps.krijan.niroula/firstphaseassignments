package com.clusus.queue;

import com.clusus.exception.EmptyQueueException;
import com.clusus.utils.StringUtils;

import java.util.Arrays;

public class DynamicSizeQueue<T> implements Queue<T> {

    protected int front;
    protected int rear;
    protected T[] elementData;

    public DynamicSizeQueue() {
        elementData = (T[]) new Object[]{};
        front = 0;
        rear = 0;
    }

    @Override
    public void enqueue(T value) {
        if (isQueueFull())
            expandQueue();
        elementData[rear] = value;
        rear++;
    }

    private void expandQueue() {
        elementData = Arrays.copyOf(elementData, rear + 1);
    }

    private boolean isQueueFull() {
        return size() == rear;
    }

    @Override
    public T deque() {
        T obj = peek();
        shrinkQueue();

        return obj;
    }

    private void shrinkQueue() {
        Object[] array = new Object[size() - 1];
        System.arraycopy(elementData, 1, array, 0, array.length);
        elementData = (T[]) array;
        rear--;
    }

    @Override
    public int size() {
        return rear;
    }

    @Override
    public T peek() {
        if (front == rear)
            throw new EmptyQueueException();
        return elementData[front];
    }

    @Override
    public String toString() {
        return StringUtils.dataStructureToString(elementData, size());
    }
}
