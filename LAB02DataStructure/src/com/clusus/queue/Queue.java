package com.clusus.queue;

public interface Queue<T> {

    void enqueue(T value);

    T deque();

    int size();

    T peek();
}
