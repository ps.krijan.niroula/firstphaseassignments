package com.clusus.queue;

import com.clusus.exception.MaximumSizeExceedException;

public class FixedSizeQueue<T> extends DynamicSizeQueue<T> {

    private final int maxSize;

    public FixedSizeQueue(int maxSize) {
        super();
        this.maxSize = maxSize;
        elementData = (T[]) new Object[maxSize];
    }

    @Override
    public void enqueue(T value) {
        if (maxSize == rear)
            throw new MaximumSizeExceedException("FixedSizeQueue Maximum size = " + maxSize);
        super.enqueue(value);
    }

}
