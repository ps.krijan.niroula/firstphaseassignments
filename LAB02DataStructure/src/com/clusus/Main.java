package com.clusus;

import com.clusus.exception.MaximumSizeExceedException;
import com.clusus.queue.DynamicSizeQueue;
import com.clusus.queue.FixedSizeQueue;
import com.clusus.queue.Queue;
import com.clusus.stack.DynamicSizeStack;
import com.clusus.stack.FixedSizeStack;
import com.clusus.stack.Stack;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Main main = new Main();
        Scanner scan = new Scanner(System.in);

        System.out.println("----Data Structure Available---");
        System.out.println("1. Stack");
        System.out.println("2. Queue");
        System.out.print("Choose one: ");
        int choice = scan.nextInt();
        switch (choice) {
            case 1:
                main.performStackOperations();
                break;
            case 2:
                main.performQueueOperations();
                break;
            default:
                System.out.println("Wrong Choice!");
                break;
        }
    }

    public void performQueueOperations() {
        Scanner scan = new Scanner(System.in);

        System.out.println("----Queue type available---");
        System.out.println("1. Dynamic Size Queue");
        System.out.println("2. Fixed Size Queue");
        System.out.print("Choose one of the queue type: ");
        int choice = scan.nextInt();
        switch (choice) {
            case 1:
                queueOperations(new DynamicSizeQueue());
                break;
            case 2:
                System.out.print("Enter the fixed size of queue: ");
                queueOperations(new FixedSizeQueue(scan.nextInt()));
                break;
            default:
                System.out.println("Wrong Choice!");
                break;
        }
    }

    public void queueOperations(Queue queue) {
        Scanner scan = new Scanner(System.in);
        char ch;
        do {
            System.out.println("\n----Queue Operations----");
            System.out.println("1. Enqueue");
            System.out.println("2. Dequeue");
            System.out.println("3. Peek head of the queue");
            System.out.println("4. Size of Queue");
            System.out.print("\nEnter a choice: ");
            int choice = scan.nextInt();
            switch (choice) {
                case 1:
                    System.out.print("Enter element to insert in the queue: ");
                    try {
                        queue.enqueue(scan.next());
                    } catch (MaximumSizeExceedException e) {
                        System.err.println("Error: " + e.getMessage());
                    }
                    break;
                case 2:
                    System.out.println("The element: " + queue.deque() + " removed from head of queue.\n");
                    break;
                case 3:
                    System.out.println("The element at head of queue without removing it is: " + queue.peek());
                    break;
                case 4:
                    System.out.println("The number of elements in the queue is: " + queue.size());
                    break;
                default:
                    System.out.println("Wrong Entry! \n ");
                    break;
            }
            System.out.println("----------------");
            System.out.println("Queue--> " + queue);
            System.out.println("----------------");
            System.out.print("\nDo you want to continue (Type y or n): ");
            ch = scan.next().charAt(0);
        } while (ch == 'Y' || ch == 'y');
    }

    public void performStackOperations() {
        Scanner scan = new Scanner(System.in);

        System.out.println("----Stack type available---");
        System.out.println("1. Dynamic Size Stack");
        System.out.println("2. Fixed Size Stack");
        System.out.print("Choose one of the stack type: ");
        int choice = scan.nextInt();
        switch (choice) {
            case 1:
                stackOperations(new DynamicSizeStack());
                break;
            case 2:
                System.out.print("Enter the fixed size of stack: ");
                stackOperations(new FixedSizeStack(scan.nextInt()));
                break;
            default:
                System.out.println("Wrong Choice!");
                break;
        }
    }

    public void stackOperations(Stack stack) {
        Scanner scan = new Scanner(System.in);
        char ch;
        do {
            System.out.println("\n----Stack Operations----");
            System.out.println("1. Push");
            System.out.println("2. Pop");
            System.out.println("3. Peek top of the stack");
            System.out.println("4. Size of Stack");
            System.out.print("\nEnter the choice: ");
            int choice = scan.nextInt();
            switch (choice) {
                case 1:
                    System.out.print("Enter element to insert in the stack: ");
                    try {
                        stack.push(scan.next());
                    } catch (MaximumSizeExceedException e) {
                        System.err.println("Error: " + e.getMessage());
                    }
                    break;
                case 2:
                    System.out.println("The element: " + stack.pop() + " removed from top of stack.\n");
                    break;
                case 3:
                    System.out.println("The element at top of stack without removing it is: " + stack.peek());
                    break;
                case 4:
                    System.out.println("The number of elements in the stack is: " + stack.size());
                    break;
                default:
                    System.out.println("Wrong Entry! \n ");
                    break;
            }
            System.out.println("----------------");
            System.out.println("Stack--> " + stack);
            System.out.println("----------------");
            System.out.print("\nDo you want to continue (Type y or n): ");
            ch = scan.next().charAt(0);
        } while (ch == 'Y' || ch == 'y');
    }

}
