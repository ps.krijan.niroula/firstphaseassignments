package com.clusus.stack;

import com.clusus.exception.MaximumSizeExceedException;

public class FixedSizeStack<T> extends DynamicSizeStack<T> {

    private final int maxSize;

    public FixedSizeStack(int maxSize) {
        super();
        this.maxSize = maxSize;
        elementData = (T[]) new Object[maxSize];
    }

    @Override
    public void push(T value) {
        if (size() >= maxSize)
            throw new MaximumSizeExceedException("FixedSizeStack Maximum size = " + maxSize);
        super.push(value);
    }

}
