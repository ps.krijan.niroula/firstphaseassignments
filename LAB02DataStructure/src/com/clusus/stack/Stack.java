package com.clusus.stack;

public interface Stack<T> {

    void push(T value);

    T pop();

    int size();

    T peek();
}
