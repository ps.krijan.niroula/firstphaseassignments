package com.clusus.stack;

import com.clusus.exception.EmptyStackException;
import com.clusus.utils.StringUtils;

import java.util.Arrays;

public class DynamicSizeStack<T> implements Stack<T> {

    protected int top;
    protected T[] elementData;

    public DynamicSizeStack() {
        elementData = (T[]) new Object[]{};
        top = 0;
    }

    private boolean isStackFull() {
        return size() == top;
    }

    @Override
    public void push(T value) {
        if (isStackFull())
            expandStack();
        elementData[top] = value;
        top++;
    }

    private void expandStack() {
        elementData = Arrays.copyOf(elementData, top + 1);
    }

    @Override
    public T pop() {
        T obj = peek();

        reduceSize();
        return obj;
    }

    private void reduceSize() {
        elementData = Arrays.copyOf(elementData, top - 1);
        top--;
    }

    @Override
    public int size() {
        return top;
    }

    @Override
    public T peek() {
        int length = size();
        if (length == 0)
            throw new EmptyStackException();
        return elementData[length - 1];
    }

    @Override
    public String toString() {
        return StringUtils.dataStructureToString(elementData, size());
    }

}
