package com.clusus.utils;

public class StringUtils {

    public static String dataStructureToString(Object[] data,int length){
        StringBuilder dataStructure = new StringBuilder("[");

        if (length > 0) {
            dataStructure.append(data[0]);
        }
        if (length > 1) {
            for (int i = 1; i <= length - 1; i++) {
                dataStructure.append(", ").append(data[i]);
            }
        }
        return dataStructure + "]";
    }
}
