package com.clusus;

import com.clusus.exception.IncorrectEquationException;

import java.util.HashMap;
import java.util.Scanner;
import java.util.Stack;

public abstract class AbstractEquationProcessor {

    protected void doOperandPush(Stack operands, String element) {
        operands.push(Double.parseDouble(element));
    }

    protected char operationsPop(Stack operations) {
        if (operations.size() == 0)
            throw new IncorrectEquationException();
        return operations.pop().toString().charAt(0);
    }

    protected double operandsPop(Stack operands) {
        if (operands.size() == 0)
            throw new IncorrectEquationException();
        return (double) operands.pop();
    }

    protected void performPopping(Stack operands, Stack operations) {
        double right = operandsPop(operands);
        double left = operandsPop(operands);
        operands.push(EquationUtils.doOperation(left, operationsPop(operations), right));
    }

    protected void checkPriority(String element, Stack operands, Stack operations) {
        if (operations.size() > 0) {
            String top = (String) operations.peek();
            if (EquationUtils.priorityLevel(element) <= EquationUtils.priorityLevel(top))
                performPopping(operands, operations);
        }
    }

    protected void doOperationPush(String element, Stack operands, Stack operations) {
        /*
         *  loop to compare element with all operations stack value
         *   (Note: not checking with all operations stack value results in mismatched mathematical operation)
         */
        for (int i = 0; i <= operands.size() + 1; i++) {
            checkPriority(element, operands, operations);
        }
        operations.push(element);
    }

    protected void checkForOperatorsSymbol(String element, Stack operands, Stack operations) {
        /*
         *  push '(' to operations
         * */
        if (element.equals("("))
            operations.push(element);

        if (element.matches("[+\\/\\-*^]"))
            doOperationPush(element, operands, operations);
    }

    /*
     *  perform remaining operations present with operands
     * */
    protected void performRemainingOperations(Stack operands, Stack operations) {
        while (operations.size() != 0) {
            performPopping(operands, operations);
        }
    }

    protected void checkBracketsException(String equation) {
        if (!EquationUtils.isSolvable(equation)) {
            throw new IncorrectEquationException();
        }
    }

    protected static String promptForValue(String element) {
        System.out.print(" Enter the value of variable " + element + ": ");
        Scanner scanner = new Scanner(System.in);
        return scanner.next();
    }

    protected String getValueForVariable(String elementString, HashMap<String, String> map) {

        /* - prompt the user for its value
         * - for same variable as before, return same value without prompting
         */
        if (map.containsKey(elementString)) {
            elementString = map.get(elementString);
        } else {
            String key = elementString;
            elementString = promptForValue(elementString);
            map.put(key, elementString);
        }

        return elementString;
    }

    protected void run(String equation, Stack operands, Stack operations) {
        checkBracketsException(equation);
        calculate();
        performRemainingOperations(operands, operations);
    }

    protected abstract void calculate();

}
