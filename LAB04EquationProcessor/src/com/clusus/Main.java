package com.clusus;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Main main = new Main();
        main.performEquationProcess();
    }

    public void performEquationProcess() {

        Scanner scan = new Scanner(System.in);
        char ch;
        do {
            System.out.println("--------Equation Solver--------");
            System.out.print(" Enter an equation you would like to solve: ");
            String equation = scan.next();
            EquationSolver processor = new EquationProcessor(equation);
            System.out.println("-------------");
            System.out.print("The result of the equation is: " + processor.getResult() + "\n");
            System.out.println("-------------");
            System.out.print("\nDo you want to solve new equation? (Type y or n): ");
            ch = scan.next().charAt(0);
        } while (ch == 'Y' || ch == 'y');
    }
}
