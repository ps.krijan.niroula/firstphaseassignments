package com.clusus;

import com.clusus.exception.IllegalOperatorException;

public class EquationUtils {

    public static double doOperation(double leftHandSide, char operator, double rightHandSize) {
        switch (operator) {
            case '^':
                return Math.pow(leftHandSide, rightHandSize);
            case '*':
                return (leftHandSide * rightHandSize);
            case '/':
                return (leftHandSide / rightHandSize);
            case '+':
                return (leftHandSide + rightHandSize);
            case '-':
                return (leftHandSide - rightHandSize);
        }
        return 0;
    }

    /*
     *  returns true if there is proper opening bracket '(' at first and closing bracket ')' afterward
     * */
    public static boolean isSolvable(String eq) {

        int parenthesisCount = 0;
        for (char element : eq.toCharArray()) {
            if (element == '(')
                parenthesisCount++;
            else if (element == ')')
                parenthesisCount--;

            if (parenthesisCount < 0)
                return false;
        }
        return parenthesisCount == 0;
    }

    /*
     *  returns priority of operators
     * */
    public static int priorityLevel(String op) {

        if (op.equals("^"))
            return 4;
        if (op.equals("*"))
            return 3;
        if (op.equals("/"))
            return 2;
        if (op.matches("[+-]"))
            return 1;

        throw new IllegalOperatorException();
    }
}
