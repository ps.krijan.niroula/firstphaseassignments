package com.clusus;

public interface EquationSolver {

    double getResult();
}
