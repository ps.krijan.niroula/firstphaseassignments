package com.clusus;

import com.clusus.exception.IncorrectEquationException;

import java.util.HashMap;
import java.util.Stack;

public class EquationProcessor extends AbstractEquationProcessor implements EquationSolver {

    private Stack operands, operations;
    private final String equation;

    public EquationProcessor(String equation) {
        this.operands = new Stack();
        this.operations = new Stack();
        this.equation = equation;
        run(equation, operands, operations);
    }

    @Override
    public double getResult() {
        if (operands.size() == 1)
            return (double) operands.peek();
        throw new IncorrectEquationException();
    }

    @Override
    protected void calculate() {

        StringBuilder bracketElement = new StringBuilder();
        StringBuilder digitBuilder = new StringBuilder();

        int count = 0;
        char[] equationCharacterArray = equation.toCharArray();
        HashMap<String, String> map = new HashMap<>();

        for (char element : equationCharacterArray) {
            count++;

            if (element == ')') {
                /*
                 * solve the brackets equation and push the result to operands
                 */
                EquationProcessor solver = new EquationProcessor(bracketElement.toString());
                operands.push(solver.getResult());

                /*
                 * pop '(' from the operations
                 */
                operations.pop();
                bracketElement = new StringBuilder();
                continue;
            }

            if (operations.size() > 0 && operations.peek().equals("(")) {

                /*
                 *  if operations have '(' as top of stack, append element to make separate equation until ')' is found
                 * */
                bracketElement.append(element);
                continue;
            }

            String elementString = String.valueOf(element);

            /*
             *  check if there is variables on equation provided or not
             */
            if (elementString.matches("[a-zA-Z]")) {
                elementString = getValueForVariable(elementString, map);
            }

            /* check if element is digit or not,
             *    - if yes: append to digitBuilder
             *    - repeat until element is non digit
             */
            if (elementString.matches("[0-9]")) {
                digitBuilder.append(elementString);
                /*
                 *  last digit must be pushed to operands for complete operations
                 */
                if (count == equationCharacterArray.length) {
                    doOperandPush(operands, digitBuilder.toString());
                }
            } else if (elementString.matches("[(+\\/\\-*^]")) {
                if (!digitBuilder.toString().equals("")) {
                    doOperandPush(operands, digitBuilder.toString());
                }
                checkForOperatorsSymbol(elementString, operands, operations);
                /*
                 * make digitBuilder empty after operands and operations pushed for next digit
                 * */
                digitBuilder = new StringBuilder();
            }
        }
    }

}
