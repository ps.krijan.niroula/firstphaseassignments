package com.clusus;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Main main = new Main();
        main.performBinaryTreeOperations();
    }

    public void performBinaryTreeOperations() {

        Scanner scan = new Scanner(System.in);
        BinaryTree binaryTree = new BinaryTree();
        char ch;
        do {
            System.out.println("\n----Binary Search Tree Operations----");
            System.out.println("1. Accept ");
            System.out.println("2. Find depth of the value");
            System.out.println("3. Depth of the binary tree");
            System.out.print("Enter a choice: ");
            int choice = scan.nextInt();
            switch (choice) {
                case 1:
                    System.out.print("Enter integer element to insert: ");
                    binaryTree.accept(scan.nextInt());
                    break;
                case 2:
                    System.out.print("Enter integer element to search and return depth: ");
                    int element = scan.nextInt();
                    System.out.print("The depth of the element = " + element + " is: " + binaryTree.depth(element) + "\n");
                    break;
                case 3:
                    System.out.print("The depth of the binary tree is: " + binaryTree.treeDepth() + "\n");
                    break;
                default:
                    System.out.println("Wrong Entry \n ");
                    break;
            }
            System.out.println("----------------");
            System.out.println(binaryTree);
            System.out.println("----------------");
            System.out.print("\nDo you want to continue (Type y or n): ");
            ch = scan.next().charAt(0);
        } while (ch == 'Y' || ch == 'y');
    }
}
