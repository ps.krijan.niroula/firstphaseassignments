package com.clusus;

public class Node {

    private int data;
    private Node left, right;

    Node(int data) {
        this.data = data;
        right = null;
        left = null;
    }

    public int getData() {
        return data;
    }

    public Node getLeft() {
        return left;
    }

    public Node getRight() {
        return right;
    }

    public boolean accept(int value) {
        if (value == data)
            return false;
        if (value > data) {
            if (right == null) {
                right = new Node(value);
                return true;
            }
            return right.accept(value);
        }
        if (value < data) {
            if (left == null) {
                left = new Node(value);
                return true;
            }
            return left.accept(value);
        }
        return false;
    }

    @Override
    public String toString() {
        return "Node{" +
                "data=" + data +
                ", left=" + left +
                ", right=" + right +
                '}';
    }
}
