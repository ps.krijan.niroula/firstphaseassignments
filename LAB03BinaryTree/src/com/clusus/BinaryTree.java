package com.clusus;

public class BinaryTree {

    private Node root;

    public boolean accept(int value) {
        if (root == null) {
            root = new Node(value);
            return true;
        }
        return root.accept(value);
    }

    public int depth(int value) {
        return depth(root, value);
    }

    private int depth(Node node, int value) {
        int depthValue = 0;
        while (node != null) {
            if (node.getData() == value) {
                return depthValue + 1;
            } else if (node.getData() > value) {
                node = node.getLeft();
                depthValue++;
            } else {
                node = node.getRight();
                depthValue++;
            }
        }
        return 0;
    }

    public int treeDepth() {
        return treeDepth(root);
    }

    private int treeDepth(Node node) {
        if (node != null) {
            int leftDepth = treeDepth(node.getLeft());
            int rightDepth = treeDepth(node.getRight());

            if (leftDepth > rightDepth)
                return leftDepth + 1;
            return rightDepth + 1;
        }
        return 0;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(root.getData() + "->[");
        builder.append(root.getLeft() + " || ");
        builder.append(root.getRight() + "]");
        return builder.toString();
    }
}
