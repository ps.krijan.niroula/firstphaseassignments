package com.clusus;

import com.clusus.exception.IllegalCharacterException;

import java.util.*;

public class RandomPasswordGenerator implements PasswordGenerator {

    private Random random;

    public RandomPasswordGenerator() {
        this.random = new Random();
    }

    @Override
    public String generate() {
        return shuffle(randomString());
    }

    private String generateRandomNumbers() {
        StringBuilder randomNumbers = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            randomNumbers.append(random.nextInt(10));
        }
        return randomNumbers.toString();
    }

    private String generateRandomUpperLetters() {

        StringBuilder randomUpperLetters = new StringBuilder();
        for (int i = 0; i < 2; i++) {
            randomUpperLetters.append(generateRandomUpperLetter());
        }
        return randomUpperLetters.toString();
    }

    private char generateRandomUpperLetter() {
        String UPPER_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        return UPPER_LETTERS.charAt(random.nextInt(26));
    }

    private String generateRandomSpecialSymbols() {

        StringBuilder randomSpecialSymbol = new StringBuilder();
        for (int i = 0; i < 2; i++) {
            randomSpecialSymbol.append(generateRandomSpecialSymbol());
        }
        return randomSpecialSymbol.toString();
    }

    private char generateRandomSpecialSymbol() {
        String SPECIAL_SYMBOL = "_$#%";
        return SPECIAL_SYMBOL.charAt(random.nextInt(4));
    }

    private String shuffle(String string) {

        Stack password = new Stack();
        Stack identityMatched = new Stack();

        /*
         *  Convert string to list of string character for using Collections.shuffle()
         * */
        List<String> letters = Arrays.asList(string.split(""));
        Collections.shuffle(letters);

        for (String letter : letters) {
            performStackOperation(letter, password, identityMatched);
        }
        /*
         *  Push remaining matched character to normal stack
         * */
        while (identityMatched.size() != 0) {
            password.push(identityMatched.pop());
        }
        return stackToString(password);
    }

    private String stackToString(Stack stack) {
        Iterator itr = stack.iterator();
        String str = "";
        while (itr.hasNext()) {
            str += itr.next().toString();
        }
        return str;
    }

    private void performStackOperation(String letter, Stack password, Stack identityMatched) {

        /*
         *  if identifyMatched stack have top element which identify doesn't match to password top element
         *    - then pop identityMatch stack element and push it to password
         * */
        if (password.size() > 0 && identityMatched.size() > 0 && !groupIdentifierStackTop(password).equals(groupIdentifierStackTop(identityMatched))) {
            password.push(identityMatched.pop());
        }

        /*
         *  if identify of letter is matched
         *     - push to identityMatched stack
         *  else
         *     - push to password stack
         * */
        if (password.size() > 0 && groupIdentifierStackTop(password).equals(groupIdentifier(letter))) {
            identityMatched.push(letter);
        } else {
            password.push(letter);
        }

    }

    private String groupIdentifierStackTop(Stack stack) {
        return groupIdentifier(stack.peek().toString());
    }

    private String groupIdentifier(String ch) {
        if (ch.matches("[A-Z]|[_$#%]")) {
            return "UPPER_AND_SYMBOL";
        }
        if (ch.matches("[0-9]")) {
            return "NUMBER";
        }
        throw new IllegalCharacterException();
    }

    private String randomString() {
        return generateRandomNumbers() + generateRandomUpperLetters() + generateRandomSpecialSymbols();
    }

}
