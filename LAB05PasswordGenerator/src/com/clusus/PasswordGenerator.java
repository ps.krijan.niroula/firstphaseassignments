package com.clusus;

public interface PasswordGenerator {

    String generate();
}
