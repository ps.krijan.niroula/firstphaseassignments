package com.clusus.squarematrixoperations;

import com.clusus.Matrix;

public abstract class AbstractSquareMatrixOperation {

    protected Matrix matrix;
    protected int size;
    protected Matrix resultMatrix;

    protected void initialize() {
        this.size = matrix.getRows();
        resultMatrix = new Matrix(size, size);
    }

    protected void exceptionCheck() {
        if (!matrix.isMatrixSquare()) throw new RuntimeException("Matrix is not square");
    }

    protected void execute() {
        initialize();
        exceptionCheck();
        performOperation();
    }

    protected abstract void performOperation();

    public abstract Object getResult();

}
