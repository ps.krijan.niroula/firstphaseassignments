package com.clusus.squarematrixoperations;

import com.clusus.Matrix;
import com.clusus.matrixoperations.SubMatrix;

public class Determinant extends AbstractSquareMatrixOperation {

    private int value;

    public Determinant(Matrix matrix) {
        if (matrix == null) throw new IllegalArgumentException();

        this.matrix = matrix;
        execute();
    }

    @Override
    protected void initialize() {
    }

    @Override
    public void performOperation() {
        value = calculateDeterminant(matrix);
    }

    private int calculateDeterminant(Matrix matrix) {
        int determinant = 0;
        if (matrix.getRows() == 2) {
            determinant = matrix.getValueAt(0, 0) * matrix.getValueAt(1, 1) - matrix.getValueAt(0, 1) * matrix.getValueAt(1, 0);
        } else {
            for (int j = 0; j < matrix.getColumns(); j++) {
                Matrix subMatrix = new SubMatrix(matrix, 1, j + 1).getResult();
                int multiplier = matrix.getValueAt(0, j);
                if (j % 2 != 0)
                    multiplier = multiplier * (-1);

                determinant += multiplier * calculateDeterminant(subMatrix);
            }
        }
        return determinant;
    }

    @Override
    public Integer getResult() {
        return value;
    }
}
