package com.clusus.squarematrixoperations;

import com.clusus.Matrix;

public class DiagonalMatrix extends AbstractSquareMatrixOperation {

    public DiagonalMatrix(Matrix matrix) {
        if (matrix == null) throw new IllegalArgumentException();

        this.matrix = matrix;
        execute();
    }

    @Override
    protected void performOperation() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (i == j)
                    resultMatrix.setValueAt(i, j, matrix.getValueAt(i, j));
            }
        }
    }

    public Matrix getResult() {
        return resultMatrix;
    }
}
