package com.clusus.squarematrixoperations;

import com.clusus.Matrix;

public class LowerTriangularMatrix extends AbstractSquareMatrixOperation {
    public LowerTriangularMatrix(Matrix matrix) {
        if (matrix == null) throw new IllegalArgumentException();

        this.matrix = matrix;
        execute();
    }

    @Override
    protected void performOperation() {
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (i >= j)
                    resultMatrix.setValueAt(i, j, matrix.getValueAt(i, j));
            }
        }
    }

    @Override
    public Matrix getResult() {
        return resultMatrix;
    }
}
