package com.clusus.matrixoperations;

import com.clusus.Matrix;

public class SubMatrix extends AbstractMatrixOperation {

    private final Matrix matrix;
    private Matrix resultMatrix;

    private final int excludingRow;
    private final int excludingColumn;

    private int rows, columns;

    public SubMatrix(Matrix matrix, int excludingRow, int excludingColumn) {
        if (matrix == null) throw new IllegalArgumentException();

        this.matrix = matrix;
        this.excludingRow = excludingRow;
        this.excludingColumn = excludingColumn;
        execute();
    }

    @Override
    protected void initialize() {
        rows = matrix.getRows();
        columns = matrix.getColumns();
        resultMatrix = new Matrix(rows - 1, columns - 1);
    }

    @Override
    protected void exceptionCheck() {
        if (excludingRow > rows || excludingColumn > columns)
            throw new RuntimeException("row or column to remove doesn't exits");
    }

    @Override
    protected void performOperation() {
        int C = 0, D = 0;
        for (int i = 0; i < rows; i++) {
            if (i == excludingRow - 1)
                continue;
            for (int j = 0; j < columns; j++) {
                if (j == excludingColumn - 1)
                    continue;
                resultMatrix.setValueAt(C, D, matrix.getValueAt(i, j));
                D++;
            }
            C++;
            D = 0;
        }
    }

    @Override
    public Matrix getResult() {
        return resultMatrix;
    }

}
