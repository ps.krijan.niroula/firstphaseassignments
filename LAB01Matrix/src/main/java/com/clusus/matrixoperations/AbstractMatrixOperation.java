package com.clusus.matrixoperations;

public abstract class AbstractMatrixOperation {

    protected abstract void exceptionCheck();

    protected abstract void initialize();

    protected abstract void performOperation();

    public abstract Object getResult();

    protected void execute() {
        initialize();
        exceptionCheck();
        performOperation();
    }
}
