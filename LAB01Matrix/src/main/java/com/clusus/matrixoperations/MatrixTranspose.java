package com.clusus.matrixoperations;

import com.clusus.Matrix;

public class MatrixTranspose extends AbstractMatrixOperation {

    private final Matrix matrix;
    private Matrix resultMatrix;
    private int rows, columns;

    public MatrixTranspose(Matrix matrix) {
        if (matrix == null) throw new IllegalArgumentException();

        this.matrix = matrix;
        execute();
    }

    @Override
    protected void exceptionCheck() {

    }

    @Override
    protected void initialize() {
        rows = matrix.getRows();
        columns = matrix.getColumns();
        resultMatrix = new Matrix(columns, rows);
    }

    @Override
    protected void performOperation() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                resultMatrix.setValueAt(j, i, matrix.getValueAt(i, j));
            }
        }
    }

    @Override
    public Matrix getResult() {
        return resultMatrix;
    }
}
