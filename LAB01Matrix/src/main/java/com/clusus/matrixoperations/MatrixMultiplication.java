package com.clusus.matrixoperations;

import com.clusus.Matrix;
import com.clusus.exception.MatrixDimensionNotMatched;

public class MatrixMultiplication extends AbstractMatrixOperation {

    private final Matrix firstMatrix;
    private final Matrix secondMatrix;
    private Matrix resultMatrix;

    public MatrixMultiplication(Matrix firstMatrix, Matrix secondMatrix) {
        if (firstMatrix == null) throw new IllegalArgumentException();
        if (secondMatrix == null) throw new IllegalArgumentException();

        this.firstMatrix = firstMatrix;
        this.secondMatrix = secondMatrix;
        execute();
    }

    @Override
    protected void initialize() {
        resultMatrix = new Matrix(firstMatrix.getRows(), secondMatrix.getColumns());
    }

    @Override
    protected void exceptionCheck() {
        if (firstMatrix.getColumns() != secondMatrix.getRows())
            throw new MatrixDimensionNotMatched("Cannot performed multiplication of two");
    }

    @Override
    protected void performOperation() {
        for (int i = 0; i < firstMatrix.getRows(); i++) {
            for (int j = 0; j < secondMatrix.getColumns(); j++) {
                for (int k = 0; k < firstMatrix.getColumns(); k++) {
                    int value = resultMatrix.getValueAt(i, j);
                    value += firstMatrix.getValueAt(i, k) * secondMatrix.getValueAt(k, j);
                    resultMatrix.setValueAt(i, j, value);
                }
            }
        }
    }

    @Override
    public Matrix getResult() {
        return resultMatrix;
    }

}
