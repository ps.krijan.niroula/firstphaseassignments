package com.clusus.matrixoperations;

import com.clusus.Matrix;
import com.clusus.exception.MatrixDimensionNotMatched;

public class MatrixAddition extends AbstractMatrixOperation {

    private final Matrix firstMatrix;
    private final Matrix secondMatrix;
    private Matrix resultMatrix;
    private int rows, columns;

    public MatrixAddition(Matrix firstMatrix, Matrix secondMatrix) {
        if (firstMatrix == null) throw new IllegalArgumentException();
        if (secondMatrix == null) throw new IllegalArgumentException();

        this.firstMatrix = firstMatrix;
        this.secondMatrix = secondMatrix;
        execute();
    }

    @Override
    protected void initialize() {
        rows = firstMatrix.getRows();
        columns = firstMatrix.getColumns();
        resultMatrix = new Matrix(rows, columns);
    }

    @Override
    protected void exceptionCheck() {
        if (secondMatrix.getRows() != firstMatrix.getRows() || secondMatrix.getColumns() != firstMatrix.getColumns())
            throw new MatrixDimensionNotMatched("Cannot performed sum of two");
    }

    @Override
    protected void performOperation() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                int sum = firstMatrix.getValueAt(i, j) + secondMatrix.getValueAt(i, j);
                resultMatrix.setValueAt(i, j, sum);
            }
        }
    }

    @Override
    public Matrix getResult() {
        return resultMatrix;
    }
}
