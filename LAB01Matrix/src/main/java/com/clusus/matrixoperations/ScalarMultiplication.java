package com.clusus.matrixoperations;

import com.clusus.Matrix;

public class ScalarMultiplication extends AbstractMatrixOperation {

    private final Matrix matrix;
    private final int scalarMultiplier;

    private Matrix resultMatrix;
    private int rows, columns;

    public ScalarMultiplication(Matrix matrix, int scalarMultiplier) {
        if (matrix == null) throw new IllegalArgumentException();

        this.matrix = matrix;
        this.scalarMultiplier = scalarMultiplier;
        execute();
    }

    @Override
    protected void exceptionCheck() {

    }

    @Override
    protected void initialize() {
        rows = matrix.getRows();
        columns = matrix.getColumns();
        resultMatrix = new Matrix(rows, columns);
    }

    @Override
    protected void performOperation() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                int value = scalarMultiplier * matrix.getValueAt(i, j);
                resultMatrix.setValueAt(i, j, value);
            }
        }
    }

    @Override
    public Matrix getResult() {
        return resultMatrix;
    }
}
