package com.clusus;

import com.clusus.matrixoperations.*;
import com.clusus.squarematrixoperations.Determinant;
import com.clusus.squarematrixoperations.DiagonalMatrix;
import com.clusus.squarematrixoperations.LowerTriangularMatrix;
import com.clusus.squarematrixoperations.UpperTriangularMatrix;

public class Matrix {
    private final int[][] data;
    private int rows, columns;

    public Matrix() {
        this(0, 0);
    }

    public Matrix(int rows, int columns) {
        this(new int[rows][columns]);
    }

    public Matrix(int[][] data) {
        if (data == null)
            throw new IllegalArgumentException();
        this.data = data;
        setRowsAndColumnsLength();
    }

    private void setRowsAndColumnsLength() {
        if (data.length == 0)
            return;
        this.rows = data.length;
        this.columns = data[0].length;
    }

    public void setValueAt(int i, int j, int value) {
        this.data[i][j] = value;
    }

    public int getValueAt(int i, int j) {
        return this.data[i][j];
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public void show() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                System.out.print(data[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public boolean isMatrixSquare() {
        return this.rows == this.columns;
    }

    public Matrix add(Matrix matrix) {
        return new MatrixAddition(this, matrix).getResult();
    }

    public Matrix multiply(Matrix matrix) {
        return new MatrixMultiplication(this, matrix).getResult();
    }

    public Matrix transpose() {
        return new MatrixTranspose(this).getResult();
    }

    public Matrix scalarMultiplication(int multiplier) {
        return new ScalarMultiplication(this, multiplier).getResult();
    }

    public Matrix subMatrix(int excludingRow, int excludingColumn) {
        return new SubMatrix(this, excludingRow, excludingColumn).getResult();
    }

    public int determinant() {
        return new Determinant(this).getResult();
    }

    public Matrix diagonalMatrix() {
        return new DiagonalMatrix(this).getResult();
    }

    public Matrix lowerTriangularMatrix() {
        return new LowerTriangularMatrix(this).getResult();
    }

    public Matrix upperTriangularMatrix() {
        return new UpperTriangularMatrix(this).getResult();
    }

}
