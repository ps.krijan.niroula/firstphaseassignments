package com.clusus;

import java.util.Scanner;

/*
 *  - Clean code
 *  - TDD
 */
public class Main {
    public static void main(String[] args) {

        Main main = new Main();
        main.performMatrixOperations();
    }

    public void performMatrixOperations() {

        Scanner scan = new Scanner(System.in);
        Matrix firstMatrix, secondMatrix, matrix;
        Matrix resultMatrix = new Matrix();
        char ch;
        do {
            System.out.println("\n----Matrix Operations----");
            System.out.println("1. Addition ");
            System.out.println("2. Multiplication ");
            System.out.println("3. Transpose ");
            System.out.println("4. Scalar Multiplication ");
            System.out.println("5. Sub Matrix ");
            System.out.println("6. Determinant ");
            System.out.println("7. Diagonal Matrix ");
            System.out.println("8. Lower Triangular Matrix ");
            System.out.println("9. Upper Triangular Matrix \n");
            System.out.print("Enter a choice: ");
            int choice = scan.nextInt();
            switch (choice) {
                case 1:
                    System.out.println("-----first matrix----");
                    firstMatrix = new Matrix(newMatrixElements(scan));
                    System.out.println("-----second matrix----");
                    secondMatrix = new Matrix(newMatrixElements(scan));
                    resultMatrix = firstMatrix.add(secondMatrix);
                    System.out.println("Sum of two matrix is : ");
                    break;
                case 2:
                    System.out.println("-----first matrix----");
                    firstMatrix = new Matrix(newMatrixElements(scan));
                    System.out.println("-----second matrix----");
                    secondMatrix = new Matrix(newMatrixElements(scan));
                    resultMatrix = firstMatrix.multiply(secondMatrix);
                    System.out.println("Multiplication of two matrix is : ");
                    break;
                case 3:
                    matrix = new Matrix(newMatrixElements(scan));
                    resultMatrix = matrix.transpose();
                    System.out.println("Transpose of the matrix is: ");
                    break;
                case 4:
                    System.out.println("Enter the scalar multiplier");
                    int multiplier = scan.nextInt();
                    matrix = new Matrix(newMatrixElements(scan));
                    resultMatrix = matrix.scalarMultiplication(multiplier);
                    System.out.println("Scalar Multiplication of matrix is: ");
                    break;
                case 5:
                    System.out.println("Enter the excluding row and column:");
                    int eRow = scan.nextInt();
                    int eColumn = scan.nextInt();
                    matrix = new Matrix(newMatrixElements(scan));
                    resultMatrix = matrix.subMatrix(eRow, eColumn);
                    System.out.println("Sub matrix excluding row " + eRow + " and column " + eColumn + "  is: ");
                    break;
                case 6:
                    System.out.println("---Enter the square matrix----");
                    matrix = new Matrix(newMatrixElements(scan));
                    int determinant = matrix.determinant();
                    resultMatrix = matrix;
                    System.out.println("Determinant of the matrix is: " + determinant);
                    break;
                case 7:
                    System.out.println("---Enter the square matrix----");
                    matrix = new Matrix(newMatrixElements(scan));
                    resultMatrix = matrix.diagonalMatrix();
                    System.out.println("The diagonal matrix is: ");
                    break;
                case 8:
                    System.out.println("---Enter the square matrix----");
                    matrix = new Matrix(newMatrixElements(scan));
                    resultMatrix = matrix.upperTriangularMatrix();
                    System.out.println("The Upper triangular matrix is: ");
                    break;
                case 9:
                    System.out.println("---Enter the square matrix----");
                    matrix = new Matrix(newMatrixElements(scan));
                    resultMatrix = matrix.lowerTriangularMatrix();
                    System.out.println("The Lower Triangular matrix is: ");
                    break;
                default:
                    System.out.println("Wrong Entry \n ");
                    break;
            }
            System.out.println("----------------");
            resultMatrix.show();
            System.out.println("----------------");
            System.out.print("\nDo you want to continue (Type y or n): ");
            ch = scan.next().charAt(0);
        } while (ch == 'Y' || ch == 'y');
    }

    public int[][] newMatrixElements(Scanner scanner) {
        System.out.println("Enter the size of rows and columns of matrix:");
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int array[][] = new int[m][n];
        System.out.println("Enter elements of the matrix:");
        for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                array[i][j] = scanner.nextInt();

        return array;
    }

}
