package com.clusus.exception;

public class MatrixDimensionNotMatched extends RuntimeException {
    public MatrixDimensionNotMatched(String message) {
        super(message);
    }
}
